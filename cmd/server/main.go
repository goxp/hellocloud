package main

import (
	"context"

	"gitlab.com/goxp/cloud0/logger"
	"gitlab.com/goxp/hellocloud/pkg/service"
)

func main() {
	logger.Init("hellocloud")
	app := service.NewService()
	ctx := context.Background()
	err := app.Start(ctx)
	if err != nil {
		logger.Tag("main").Error(err)
	}
}
