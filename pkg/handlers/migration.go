package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/goxp/hellocloud/pkg/datastore"
	"gorm.io/gorm"
)

type MigrationHandler struct {
	db *gorm.DB
}

func NewMigrationHandler(db *gorm.DB) *MigrationHandler {
	return &MigrationHandler{db: db}
}


func (h *MigrationHandler) Migrate(ctx *gin.Context) {
	models := []interface{}{
		&datastore.Item{},
	}

	for _, m := range models {
		err := h.db.AutoMigrate(m)
		if err != nil {
			_ = ctx.Error(err)
			return
		}
	}
}

// Seed seeds some records in DB
func (h *MigrationHandler) Seed(ctx *gin.Context) {
	bytes, err := ioutil.ReadFile("resources/items.json")
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	var data []*datastore.Item
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	for _, item := range data {
		if item.State == 0 {
			item.State = datastore.StateNew
		}
	}

	err = h.db.Create(data).Error
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	ctx.Status(http.StatusNoContent)
}
