package handlers

import (
	"net/http"

	"gitlab.com/goxp/cloud0/ginext"
	"gitlab.com/goxp/cloud0/logger"
	"gitlab.com/goxp/hellocloud/pkg/datastore"
)

type ItemHandlers struct {
	repo *datastore.ItemRepo
}

type ItemCreateRequest struct {
	Name        string `json:"name" validate:"required"`
	Description string `json:"description"`
	Tags        string `json:"tags"`
}

type ItemCreateResponse struct {
	ID uint `json:"id"`
}

type ItemListRequest struct {
	Name  string `form:"name"`
	State byte   `form:"state"`
	Tags  string `form:"tags"`
	Owner uint   `form:"owner"`
}

type ItemUpdateRequest struct {
	ID          uint   `uri:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Tags        string `json:"tags"`
}

func NewItemHandlers(repo *datastore.ItemRepo) *ItemHandlers {
	return &ItemHandlers{repo: repo}
}

func (h *ItemHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	owner := uint(r.Uint64UserID())
	req := &ItemCreateRequest{}
	r.MustBind(req)

	item := &datastore.Item{
		Name:        req.Name,
		Description: req.Description,
		State:       datastore.StateNew,
		Tags:        req.Tags,
		Owner:       owner,
	}

	r.MustNoError(h.repo.Create(r.Context(), item))

	return ginext.NewResponseData(http.StatusCreated, &ItemCreateResponse{ID: item.ID}).WithResponseCode("00S200"), nil
}

func (h *ItemHandlers) List(r *ginext.Request) (*ginext.Response, error) {

	req := ItemListRequest{}
	r.MustBind(&req)

	// log := logger.WithCtx(r.Context(), "ItemHandler.List")

	filter := &datastore.ItemFilter{
		Name:  req.Name,
		State: req.State,
		Tags:  req.Tags,
		Owner: req.Owner,
		Pager: ginext.NewPagerWithGinCtx(r.GinCtx),
	}

	result, err := h.repo.Filter(r.Context(), filter)
	r.MustNoError(err)
	resp := ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager)

	return resp, nil
}

func (h *ItemHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "ItemHandlers.Update")

	req := ItemUpdateRequest{}

	r.MustBindUri(&req)
	r.MustBind(&req)

	// require owner to update if need
	currentUser := uint(r.Uint64UserID())
	item, err := h.repo.Get(r.Context(), req.ID)
	if err != nil {
		l.WithError(err).WithField("item.id", req.ID).Error("failed to query item")
		return nil, ginext.NewError(http.StatusInternalServerError, "Server error")
	}

	if item.Owner != uint(currentUser) {
		return nil, ginext.NewError(http.StatusForbidden, "unauthorized to modify this item")
	}

	update := &datastore.Item{
		Name:        req.Name,
		Tags:        req.Tags,
		Description: req.Tags,
	}

	err = h.repo.Update(r.Context(), req.ID, update)
	respErr := ginext.NewErrorCode(500, err.Error(), "00E500")
	return ginext.NewResponse(http.StatusOK, ginext.WithResponseCodeOption("00200")), respErr
}
