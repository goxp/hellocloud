package datastore

import (
	"context"
	"strings"
	"time"

	"gitlab.com/goxp/cloud0/ginext"
	"gitlab.com/goxp/cloud0/logger"
	"gorm.io/gorm"
)

const (
	StateNew byte = iota + 1 // starts from 1
	StateDoing
	StateDone

	generalQueryTimeout = 2 * time.Second
)

type Item struct {
	gorm.Model
	Name        string
	Description string
	State       byte
	Tags        string
	Owner       uint
}

func (im *Item) GetSortableFields() []string {
	return []string{"id", "name", "state", "owner", "created_at"}
}

var (
	_ ginext.SortableFieldsGetter = &Item{} // compile-time validate model adopting SortableFieldsGetter
)

type ItemFilter struct {
	Name  string
	State byte
	Tags  string
	Owner uint
	Pager *ginext.Pager
}

type ItemFilterResult struct {
	Filter  *ItemFilter
	Records []*Item
}

type ItemRepo struct {
	db    *gorm.DB
	debug bool
}

func NewItemRepo(db *gorm.DB) *ItemRepo {
	return &ItemRepo{db: db}
}

func (r *ItemRepo) DBWithTimeout(ctx context.Context) (*gorm.DB, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(ctx, generalQueryTimeout)
	return r.db.WithContext(ctx), cancel
}

func (r *ItemRepo) Create(ctx context.Context, item *Item) error {
	tx, cancel := r.DBWithTimeout(ctx)
	defer cancel()
	return tx.Create(item).Error
}

func (r *ItemRepo) Get(ctx context.Context, id uint) (*Item, error) {
	o := &Item{}
	tx, cancel := r.DBWithTimeout(ctx)
	defer cancel()
	err := tx.First(&o, id).Error
	return o, err
}

func (r *ItemRepo) Filter(ctx context.Context, f *ItemFilter) (*ItemFilterResult, error) {

	tx, cancel := r.DBWithTimeout(ctx)
	defer cancel()

	tx = tx.Model(&Item{})
	log := logger.WithCtx(ctx, "ItemRepo.Filter")

	if f.Name != "" {
		tx = tx.Where("LOWER(name) LIKE ?", "%"+strings.ToLower(f.Name)+"%")
	}
	if f.State > 0 {
		tx = tx.Where("state = ?", f.State)
	}
	if f.Tags != "" {
		tx = tx.Where("tags LIKE ?", "%"+f.Tags+"%")
	}
	if f.Owner > 0 {
		tx = tx.Where("owner = ?", f.Owner)
	}

	result := &ItemFilterResult{
		Filter:  f,
		Records: []*Item{},
	}

	pager := result.Filter.Pager

	tx = pager.DoQuery(&result.Records, tx)
	if tx.Error != nil {
		log.WithError(tx.Error).Error("error while filter items")
	}

	return result, tx.Error
}

func (r *ItemRepo) Update(ctx context.Context, id uint, update *Item) error {
	current, err := r.Get(ctx, id)
	if err != nil {
		return err
	}

	if update.Name != "" {
		current.Name = update.Name
	}
	if update.State != 0 {
		current.State = update.State
	}
	if update.Tags != "" {
		current.Tags = update.Tags
	}
	if update.Description != "" {
		current.Description = update.Description
	}
	tx, cancel := r.DBWithTimeout(ctx)
	defer cancel()

	return tx.Save(&current).Error
}
