package service

import (
	"github.com/caarlos0/env/v6"
	"gitlab.com/goxp/cloud0/ginext"
	"gitlab.com/goxp/cloud0/service"
	"gitlab.com/goxp/hellocloud/pkg/datastore"
	"gitlab.com/goxp/hellocloud/pkg/handlers"
)

type extraSetting struct {
	DbDebugEnable bool `env:"DB_DEBUG_ENABLE" envDefault:"false"`
}

type Service struct {
	*service.BaseApp
	setting *extraSetting
}

func NewService() *Service {
	s := &Service{
		service.NewApp("hellocloud", "v1.0"),
		&extraSetting{},
	}

	_ = env.Parse(s.setting)

	db := s.GetDB()
	if s.setting.DbDebugEnable {
		db = db.Debug()
	}

	itemsRepo := datastore.NewItemRepo(db)
	itemsHandlers := handlers.NewItemHandlers(itemsRepo)

	s.Router.GET("/items", ginext.WrapHandler(itemsHandlers.List))
	s.Router.POST("/items", ginext.AuthRequiredMiddleware, ginext.WrapHandler(itemsHandlers.Create))
	s.Router.PUT("/items/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(itemsHandlers.Update))

	migrateHandler := handlers.NewMigrationHandler(db)
	s.Router.POST("/internal/migrate", migrateHandler.Migrate)
	s.Router.POST("/internal/seed", migrateHandler.Seed)

	return s
}
