module gitlab.com/goxp/hellocloud

go 1.16

// replace gitlab.com/goxp/cloud0 => ../cloud0

require (
	github.com/caarlos0/env/v6 v6.6.2
	github.com/gin-gonic/gin v1.7.2
	gitlab.com/goxp/cloud0 v1.6.0
	gorm.io/gorm v1.21.11
)
