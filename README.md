# HelloCloud

---

A simple microservice that's built on cloud0 toolkit.

## Get started

Run with docker-compose 

```
docker-compose up -d
```

After starting success, the app exposes to port 8088. For the first time, 
you need to call migration & seeding test data:

```
curl -X POST http://localhost:8088/internal/migrate
curl -X POST http://localhost:8088/internal/seed
```

Then you could try some functions

- list items

```shell
curl 'http://localhost:8088/items?page_size=10&order=id'
```

- create items

```shell
curl -X POST -H 'x-user-id: 11' 'http://localhost:8088/items' \
  -d '{"name": "test task"}'
```

- update items

```shell
curl -X PUT -H 'x-user-id: 7' 'http://localhost:8088/items/7' \
  -d '{"name": "updated items"}'
```
